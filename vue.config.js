module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160252/learn_bootstrap/'
    : '/'
}
